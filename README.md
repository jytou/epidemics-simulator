Simulating complex epidemics scenarios.
Here are the main features:
- the terrain consists of points which store the current level of contamination, it is possible to parametrize the duration of contamination as well as rate of spread to neighboring points,
- there is the possibility to add enclosed spaces (currently rectangular) which stop or limit spread,
- individuals evoluate in the environment using “routines” which condition their movements within the environment,
- individuals can get contaminated with a probability that depends on the level of contamination of the current point their are on as well as their immunity to the illness,
- individuals who have been contaminated can get immune for some time.

Everything is visualized within a frame which also shows the graph of contamination level.

TODO list:
- birth and death of individuals,
- new routines (daily routines, family routines, work routines, etc.)
- transportation,
- level of immune system of an individual,
- ageing,
- more to come.