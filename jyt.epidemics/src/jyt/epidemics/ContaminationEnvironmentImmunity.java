package jyt.epidemics;

public class ContaminationEnvironmentImmunity implements IContamination
{
	private double mContaminationFactor;

	public ContaminationEnvironmentImmunity(double pContaminationFactor)
	{
		mContaminationFactor = pContaminationFactor;
	}

	@Override
	public void contaminate(Individual pIndividual, Environment pEnvironment)
	{
		final int locX = (int)pIndividual.getPosition().getX();
		final int locY = (int)pIndividual.getPosition().getY();
		if (pIndividual.getContagionTime() == -1)
		{
			if (Epidemics.sRand.nextDouble() / mContaminationFactor < pEnvironment.getSpace()[locX][locY] * (1 - pIndividual.getImmunity() * pIndividual.getImmunity()))
			{
				// contaminated!
				pIndividual.setContagionTime(pEnvironment.getTime());
				pIndividual.setContagious(1);
				pIndividual.setImmuneTime(-1);
				pIndividual.setImmunity(0);
			}
		}
	}
}
