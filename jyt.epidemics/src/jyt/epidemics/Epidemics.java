package jyt.epidemics;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Epidemics extends JFrame
{
	public static final int SIM_WIDTH = 1440;
	public static final int SIM_HEIGHT = 978;

	public enum ValueType
	{
		CONTAMINATED(Color.red),
		IMMUNE(Color.cyan),
		TOTAL(Color.black),
		OTHERS(Color.green);
		private Color mColor;
		private ValueType(Color pColor)
		{
			mColor = pColor;
		}
		public Color getColor()
		{
			return mColor;
		}
	};
	public static final Random sRand = new Random();
	private Environment mEnvironment;
	private SortedMap<Integer, Map<ValueType, Double>> mHistory = new TreeMap<>();

	public class MyPanel extends JPanel
	{
		@Override
		protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			final BufferedImage bufferedImage = new BufferedImage(SIM_WIDTH, SIM_HEIGHT, BufferedImage.TYPE_3BYTE_BGR);
			final WritableRaster r = bufferedImage.getRaster();
			final int[] img = new int[SIM_WIDTH * SIM_HEIGHT * 3];
			synchronized (Epidemics.this)
			{
				final double[][] space = mEnvironment.getSpace();
				for (int y = 0; y < SIM_HEIGHT; y++)
				{
					final int yy = y * SIM_WIDTH * 3;
					for (int x = 0; x < SIM_WIDTH; x++)
					{
						img[yy + x * 3] = (int)(Math.min(255, space[x][y] * 500));
						img[yy + x * 3 + 1] = 0;
						img[yy + x * 3 + 2] = 0;
					}
				}
				int nbImmune = 0;
				int nbContaminated = 0;
				int nbTotalImact = 0;
				for (Individual individual : mEnvironment.getIndividuals())
				{
					final int base = (((int)individual.getPosition().getY()) * SIM_WIDTH + (int)individual.getPosition().getX()) * 3;
					img[base + 0] = (int)(individual.getContagious() * 255);
					img[base + 1] = (int)((1 - individual.getContagious()) * 255);
					img[base + 2] = (int)(individual.getImmunity() * 255);
					if (individual.getContagionTime() >= 0)
						nbContaminated++;
					if (individual.getImmuneTime() >= 0)
						nbImmune++;
					if (individual.isHasBeenContaminated())
						nbTotalImact++;
				}
				r.setPixels(0, 0, SIM_WIDTH, SIM_HEIGHT, img);
				g.drawImage(bufferedImage, 0, 0, null);
				g.setColor(Color.white);
				mEnvironment.getLocations().values().forEach(l -> g.drawRect((int)l.getLocation().getX(), (int)l.getLocation().getY(), (int)l.getLocation().getWidth(), (int)l.getLocation().getHeight()));
				g.setColor(Color.black);
				int yyy = 15;
				final int graphHeight = 500;
				g.drawLine(SIM_WIDTH + 10, yyy, SIM_WIDTH + 10, yyy + graphHeight);
				g.drawLine(SIM_WIDTH + 10, yyy + graphHeight, getWidth(), yyy + graphHeight);
				for (ValueType vt : ValueType.values())
				{
					Double previousValue = null;
					g.setColor(vt.getColor());
					for (Entry<Integer, Map<ValueType, Double>> entry : mHistory.entrySet())
					{
						final Double value = entry.getValue().get(vt);
						if (previousValue != null)
							g.drawLine(SIM_WIDTH + 10 + entry.getKey() - 1, (int)(yyy + graphHeight - 1.0 * graphHeight * previousValue / 100), SIM_WIDTH + 10 + entry.getKey(), (int)(yyy + graphHeight - 1.0 * graphHeight * value / 100));
						previousValue = value;
					}
				}
				g.setColor(Color.black);
				yyy += graphHeight + 15;
				g.drawString("Time " + mEnvironment.getTime(), SIM_WIDTH + 10, yyy);
				yyy = drawString(g, "Contaminated", nbContaminated, yyy += 15);
				yyy = drawString(g, "Immune", nbImmune, yyy);
				final int nbOthers = mEnvironment.getIndividuals().size() - nbContaminated - nbImmune;
				yyy = drawString(g, "Others", nbOthers, yyy);
				yyy = drawString(g, "Total Impact", nbTotalImact, yyy);
			}
		}

		private int drawString(Graphics g, String pText, int pValue, int pPosition)
		{
			g.drawString(pText + " " + pValue + " (" + (100.0 * pValue / mEnvironment.getIndividuals().size()) + "%)", SIM_WIDTH + 10, pPosition);
			return pPosition + 15;
		}
	}

	public Epidemics()
	{
		super("Epidemics");
		addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				super.windowClosing(e);
				System.exit(0);
			}
		});
		setSize(SIM_WIDTH + 230, SIM_HEIGHT + 50);
		mEnvironment = new Environment(SIM_WIDTH, SIM_HEIGHT);
		mEnvironment.getLocations().put("work", new Location("work", new Rectangle(100, 100, 200, 150), 0));
		final double[][] walls = mEnvironment.getWalls();
		for (Location location : mEnvironment.getLocations().values())
		{
			final int locationFirstX = (int)location.getLocation().getX();
			final int locationSecondX = (int)(locationFirstX + location.getLocation().getWidth());
			final int locationFirstY = (int)location.getLocation().getY();
			final int locationSecondY = (int)(locationFirstY + location.getLocation().getHeight());
			for (int y = locationFirstY; y < locationSecondY; y++)
			{
				walls[locationFirstX][y] = location.getOpenness();
				walls[locationSecondX][y] = location.getOpenness();
			}
			for (int x = locationFirstX; x < locationSecondX; x++)
			{
				walls[x][locationFirstY] = location.getOpenness();
				walls[x][locationSecondY] = location.getOpenness();
			}
		}
		final IRoutine randomRoutine = new RoutineRandom();
		for (int i = 0; i < 10000; i++)
			mEnvironment.getIndividuals().add(new Individual(new Point(sRand.nextInt(SIM_WIDTH), sRand.nextInt(SIM_HEIGHT)), sRand.nextInt(100), randomRoutine));
		for (int i = 0; i < mEnvironment.getIndividuals().size() / 1000; i++)
		{
			final Individual individual = mEnvironment.getIndividuals().get(i);
			individual.setContagionTime(0);
			individual.setContagious(sRand.nextDouble());
		}
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(new MyPanel());
		setVisible(true);
		final IIllnessIndividuals illness = new IllnessIndividualsCovid19();
		final IIllnessDynamics illnessDynamics = new IllnessDynamicsFaintAndSpread(0.015, 0.8);
		final IContamination contamination = new ContaminationEnvironmentImmunity(0.1);
		new Thread()
		{
			public void run()
			{
				while (true)
				{
					try
					{
						Thread.sleep(10);
					}
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
					for (int iter = 0; iter < 100; iter++)
						synchronized (Epidemics.this)
						{
							illnessDynamics.applyDynamics(mEnvironment);
							final double[][] space = mEnvironment.getSpace();
							for (Individual individual : mEnvironment.getIndividuals())
							{
								illness.applyIllness(individual, mEnvironment.getTime());
								contamination.contaminate(individual, mEnvironment);
								space[(int)individual.getPosition().getX()][(int)individual.getPosition().getY()] += individual.getContagious();
							}
							for (int y = 0; y < SIM_HEIGHT; y++)
								for (int x = 0; x < SIM_WIDTH; x++)
									if (space[x][y] > 1)
										space[x][y] = 1;
							for (Individual individual : mEnvironment.getIndividuals())
								individual.getRoutine().apply(individual, mEnvironment.getTime());
							if (mEnvironment.getTime() % 100 == 0)
							{
								final HashMap<ValueType, Double> values = new HashMap<>();
								mHistory.put((int)(mEnvironment.getTime() / 100), values);
								int nbImmune = 0;
								int nbContaminated = 0;
								int nbTotalImact = 0;
								for (Individual individual : mEnvironment.getIndividuals())
								{
									if (individual.getContagionTime() >= 0)
										nbContaminated++;
									if (individual.getImmuneTime() >= 0)
										nbImmune++;
									if (individual.isHasBeenContaminated())
										nbTotalImact++;
								}
								final int nbOthers = mEnvironment.getIndividuals().size() - nbContaminated - nbImmune;
								values.put(ValueType.CONTAMINATED, 100.0 * nbContaminated / mEnvironment.getIndividuals().size());
								values.put(ValueType.IMMUNE, 100.0 * nbImmune / mEnvironment.getIndividuals().size());
								values.put(ValueType.OTHERS, 100.0 * nbOthers / mEnvironment.getIndividuals().size());
								values.put(ValueType.TOTAL, 100.0 * nbTotalImact / mEnvironment.getIndividuals().size());
							}
							mEnvironment.tick();
						}
					repaint();
				}
			}
		}.start();
	}

	public static void main(String[] args)
	{
		new Epidemics();
	}
}
