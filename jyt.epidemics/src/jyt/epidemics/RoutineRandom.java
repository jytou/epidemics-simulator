package jyt.epidemics;

import java.awt.geom.Point2D;

public class RoutineRandom implements IRoutine
{
	@Override
	public void apply(Individual pIndividual, long pTime)
	{
		final Point2D oldSpeed = pIndividual.getSpeed();
		double newSpeedX = Math.min(1, Math.max(-1, oldSpeed.getX() + Epidemics.sRand.nextDouble() - 0.5));
		double newSpeedY = Math.min(1, Math.max(-1, oldSpeed.getY() + Epidemics.sRand.nextDouble() - 0.5));
		final Point2D oldPos = pIndividual.getPosition();
		double newPosX = oldPos.getX() + newSpeedX;
		if (newPosX < 0)
		{
			newPosX = -newPosX;
			newSpeedX = -newPosX;
		}
		else if (newPosX >= Epidemics.SIM_WIDTH)
		{
			newPosX = Epidemics.SIM_WIDTH * 2 - newPosX;
			newSpeedX = -newSpeedX;
		}
		double newPosY = oldPos.getY() + newSpeedY;
		if (newPosY < 0)
		{
			newPosY = -newPosY;
			newSpeedY = -newSpeedY;
		}
		else if (newPosY >= Epidemics.SIM_HEIGHT)
		{
			newPosY = Epidemics.SIM_HEIGHT * 2 - newPosY;
			newSpeedY = -newSpeedY;
		}
		pIndividual.setSpeed(new Point2D.Double(newSpeedX, newSpeedY));
		pIndividual.setPosition(new Point2D.Double(newPosX, newPosY));
	}
}
