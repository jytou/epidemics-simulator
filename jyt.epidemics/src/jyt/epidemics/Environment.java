package jyt.epidemics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Environment
{
	private int mWidth;
	private int mHeight;
	private double[][] mSpace;
	private double[][] mWalls;
	private List<Individual> mIndividuals = new ArrayList<>();
	private Map<String, Location> mLocations = new HashMap<>();
	private long mTime;

	public Environment(int pWidth, int pHeight)
	{
		super();
		mWidth = pWidth;
		mHeight = pHeight;
		mSpace = new double[pWidth][];
		mWalls = new double[pWidth][];
		for (int i = 0; i < mSpace.length; i++)
		{
			mSpace[i] = new double[pHeight];
			mWalls[i] = new double[pHeight];
			Arrays.fill(mWalls[i], 1.0);
		}
	}

	public long getTime()
	{
		return mTime;
	}

	public void tick()
	{
		mTime++;
	}

	public int getWidth()
	{
		return mWidth;
	}

	public int getHeight()
	{
		return mHeight;
	}

	public double[][] getSpace()
	{
		return mSpace;
	}

	public double[][] getWalls()
	{
		return mWalls;
	}

	public List<Individual> getIndividuals()
	{
		return mIndividuals;
	}

	public Map<String, Location> getLocations()
	{
		return mLocations;
	}
}
