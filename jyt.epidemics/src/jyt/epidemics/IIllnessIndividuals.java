package jyt.epidemics;

public interface IIllnessIndividuals
{
	void applyIllness(Individual pIndividual, long pTime);
}
