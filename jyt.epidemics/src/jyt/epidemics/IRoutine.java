package jyt.epidemics;

public interface IRoutine
{
	void apply(Individual pIndividual, long pTime);
}
