package jyt.epidemics;

import java.awt.Point;
import java.awt.geom.Point2D;

public class Individual
{
	private Point2D mPosition = new Point2D.Double();
	private Point2D mSpeed = new Point2D.Double();
	private double mAge;
	private long mImmuneTime;
	private double mImmunity;
	private long mContagionTime;
	private double mContagious;
	private double mSick;
	private double mImmuneSystem;
	private IRoutine mRoutine;
	private boolean mHasBeenContaminated = false;

	public Individual(Point2D pPosition, double pAge, IRoutine pRoutine)
	{
		super();
		mPosition = pPosition;
		mSpeed = new Point();
		mAge = pAge;
		mImmuneTime = -1;
		mImmunity = 0;
		mContagionTime = -1;
		mContagious = 0;
		mSick = 0;
		mRoutine = pRoutine;
		mImmuneSystem = Epidemics.sRand.nextDouble();
	}

	public Point2D getPosition()
	{
		return mPosition;
	}

	public void setPosition(Point2D pPosition)
	{
		mPosition = pPosition;
	}

	public Point2D getSpeed()
	{
		return mSpeed;
	}

	public void setSpeed(Point2D pSpeed)
	{
		mSpeed = pSpeed;
	}

	public double getAge()
	{
		return mAge;
	}

	public void setAge(double pAge)
	{
		mAge = pAge;
	}

	public long getImmuneTime()
	{
		return mImmuneTime;
	}

	public void setImmuneTime(long pImmuneTime)
	{
		mImmuneTime = pImmuneTime;
	}

	public double getImmunity()
	{
		return mImmunity;
	}

	public void setImmunity(double pImmunity)
	{
		mImmunity = pImmunity;
	}

	public long getContagionTime()
	{
		return mContagionTime;
	}

	public void setContagionTime(long pContagionTime)
	{
		mContagionTime = pContagionTime;
		mHasBeenContaminated |= (pContagionTime != -1);
	}

	public double getContagious()
	{
		return mContagious;
	}

	public void setContagious(double pContagious)
	{
		mContagious = pContagious;
	}

	public double getSick()
	{
		return mSick;
	}

	public void setSick(double pSick)
	{
		mSick = pSick;
	}

	public double getImmuneSystem()
	{
		return mImmuneSystem;
	}

	public IRoutine getRoutine()
	{
		return mRoutine;
	}

	public boolean isHasBeenContaminated()
	{
		return mHasBeenContaminated;
	}
}