package jyt.epidemics;

public interface IContamination
{
	void contaminate(Individual pIndividual, Environment pEnvironment);
}
