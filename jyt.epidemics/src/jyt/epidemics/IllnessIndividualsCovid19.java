package jyt.epidemics;

public class IllnessIndividualsCovid19 implements IIllnessIndividuals
{
	@Override
	public void applyIllness(Individual pIndividual, long pTime)
	{
		if (pIndividual.getContagionTime() >= 0)
		{
			pIndividual.setContagious(pIndividual.getContagious() / (1.0 + Epidemics.sRand.nextDouble() / 100));
			if (pIndividual.getContagious() < 0.01)
			{
				pIndividual.setContagious(0);
				pIndividual.setContagionTime(-1);
				pIndividual.setImmuneTime(pTime);
				pIndividual.setImmunity(1);
			}
		}
		else
		{
			if (pIndividual.getImmuneTime() >= 0)
			{
				pIndividual.setImmunity(pIndividual.getImmunity() / (1.0 + Epidemics.sRand.nextDouble() / 100000));
				if (pIndividual.getImmunity() < 0.001)
				{
					pIndividual.setImmuneTime(-1);
					pIndividual.setImmunity(0);
				}
			}
		}
	}

}
