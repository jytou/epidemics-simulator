package jyt.epidemics;

import java.awt.geom.Rectangle2D;

public class Location
{
	private String mName;
	private Rectangle2D mLocation;
	private double mClosedness;// 0 for open, 1 for closed

	public Location(String pName, Rectangle2D pLocation, double pClosedness)
	{
		super();
		mName = pName;
		mLocation = pLocation;
		mClosedness = pClosedness;
	}

	public String getName()
	{
		return mName;
	}

	public Rectangle2D getLocation()
	{
		return mLocation;
	}

	public double getOpenness()
	{
		return mClosedness;
	}

}