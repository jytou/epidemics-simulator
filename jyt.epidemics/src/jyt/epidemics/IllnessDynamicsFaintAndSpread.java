package jyt.epidemics;

public class IllnessDynamicsFaintAndSpread implements IIllnessDynamics
{
	private double mSpreadRate;
	private double mFaintRate;

	public IllnessDynamicsFaintAndSpread(double pSpreadRate, double pFaintRate)
	{
		super();
		mSpreadRate = pSpreadRate;
		mFaintRate = pFaintRate;
	}

	@Override
	public void applyDynamics(Environment pEnvironment)
	{
		// spread the illness in the environment
		final double[][] space = pEnvironment.getSpace();
		final double[][] walls = pEnvironment.getWalls();
		for (int y = 0; y < pEnvironment.getHeight(); y++)
			for (int x = 0; x < pEnvironment.getWidth(); x++)
				space[x][y] *= mFaintRate;
		for (int y = 0; y < pEnvironment.getHeight(); y++)
			for (int x = 0; x < pEnvironment.getWidth(); x++)
				for (int ity = -1; ity <= 1; ity++)
				{
					final int yy = y + ity;
					if ((yy >= 0) && (yy < pEnvironment.getHeight()))
						for (int itx = -1; itx <= 1; itx++)
						{
							final int xx = x + itx;
							if ((xx >= 0) && (xx < pEnvironment.getWidth()))
								space[xx][yy] += space[x][y] * walls[xx][yy] * mSpreadRate;
						}
				}
	}
}
